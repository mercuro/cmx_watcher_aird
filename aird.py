# Libraries
import os
import pickle
import socket
import sys
from pprint import pprint

from environs import Env
import time
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
import xmltodict
import json
import re
import requests
import logging
import datetime
import subprocess

####################################################
# Read .env into os.environ
env = Env()
env.read_env()

############## USER DEFINED SETTINGS ###############
validator = env("AIRD_VALIDATOR")
secret = env("AIRD_SECRET")
netxml_dir = env("AIRD_NETXMLDIR")
last_content = None

class Watcher:
    DIRECTORY_TO_WATCH = os.path.join(env("AIRD_NETXMLDIR"))
    airodump_process = None

    def __init__(self):
        self.observer = Observer()

    def run(self):
        # print(f"[{datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')}] AirD Armed.")
        print("[%s] AirD Armed."
              % (datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')))

        self.airodump_process = subprocess.Popen(
            ["sudo",
             "airodump-ng",
             "--manufacturer",
             "--uptime",
             "-w",
             self.DIRECTORY_TO_WATCH + "clients_`date +%Y-%m-%d.%H:%M:%S`.netxml",
             "--output-format",
             "netxml",
             "--write-interval",
             "20",
             "wlan1mon"],
            shell=True, stderr=subprocess.PIPE)
        # print(f"[{datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')}] airodump-ng: {self.airodump_process.returncode}")
        print("[%s] airodump-ng: %s"
              % (datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), self.airodump_process.returncode))


        event_handler = Handler()
        event_handler.airodump_process = self.airodump_process
        self.observer.schedule(event_handler, self.DIRECTORY_TO_WATCH, recursive=True)
        self.observer.start()
        try:
            while True:
                time.sleep(5)
        except:
            self.observer.stop()
            print ("Error")

        self.observer.join()

escape_illegal_xml_characters = lambda x: re.sub(u'&#x {1,}[0-9a-z]+;', '', x)

class Handler(FileSystemEventHandler):
    airodump_process = None

    @staticmethod
    def on_any_event(event):
        if event.is_directory:
            return None

        elif event.event_type == 'created':
            # Take any action here when a file is first created.
            # print (
            #     f"[{datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')}] Received created event - {event.src_path}.")
            print ("[%s] Received created event - %s"
                   % (datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), event.src_path))
            Handler.post_new_data (event.src_path)

        elif event.event_type == 'modified':
            # Taken any action here when a file is modified.
            # print(
            #     f"[{datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')}] Received modified event - {event.src_path}.")
            print ("[%s] Received modified event - %s"
                   % (datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), event.src_path))
            Handler.post_new_data (event.src_path)

    @staticmethod
    def post_new_data(src_path):
        global last_content
        if (src_path.endswith('.netxml')):
            try:
                # print(f"[{datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')}] Processing file: {src_path}")
                time.sleep(2)
                with open(src_path) as xml_file:

                    content = escape_illegal_xml_characters(xml_file.read())
                    if ((len(content) > 0) & (content != last_content)):
                        print("[%s] Processing file: %s"
                              % (datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), src_path))
                        logging.debug("Processing file: ", src_path)

                        my_dict = xmltodict.parse(content)
                        json_data = json.dumps(my_dict)
                        # delta_json = Handler.parse_delta(my_dict, xmltodict.parse(last_content))
                        if (last_content):
                            delta_json = json.dumps(Handler.parse_delta(my_dict, xmltodict.parse(last_content)));
                        else:
                            delta_json = json_data

                        # print(f"[{datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')}] Posting to {env('AIRD_POSTURL')}")
                        print("[%s] Posting to: %s (%d)"
                              % (datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), env('AIRD_POSTURL'),
                                 sys.getsizeof(delta_json)))
                              # % (datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), env('AIRD_POSTURL'), sys.getsizeof(json_data)))
                        # tmp = open(src_path + datetime.datetime.now().strftime('%Y-%m-%d_%H%M%S') + '.tmp', 'w')
                        # tmp.write(json_data)
                        # tmp.close()
                        s = requests.Session()
                        r = s.post(
                            env('AIRD_POSTURL'),
                            data={'scan': delta_json},
                            headers={
                                'user-agent': 'aird-agent/' + socket.gethostname() + '/0.0.2',
                                'Accept': 'application/json',
                                'Authorization': 'Bearer ' + env('AIRD_SECRET')
                            },
                            timeout=30
                        )

                        logging.debug("Post result (", r.url, "): ", r.status_code)
                        # print(
                        #     f"[{datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')}] Post result ({r.url}):  {r.status_code}")
                        print("[%s] Post result: %d"
                              % (datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), r.status_code))

                        if (r.status_code == 200):
                            last_content = content
                            # tmp = open('lc_' + datetime.datetime.now().strftime('%Y-%m-%d_%H%M%S') + '.tmp', 'w')
                            # tmp.write(last_content)
                            # tmp.close()

                            # fp = open(src_path, 'a')
                            # fp.truncate(0)
                            # fp.close()
                        else:
                            # self.airodump_process.kill()
                            # print(f"[{datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')}] Error posting. Need to restart")
                            print("[%s] Error posting. Need to restart"
                                  % (datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')))
                        del s
            except Exception as e:
                print("[%s] Error: %s"
                      % (datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), repr(e) ))
                logging.error(repr(e))

    @staticmethod
    def parse_delta(content_a, content_b):
        # print(type(content_b['detection-run']['wireless-network']))
        print("Original update size: ", len(content_a['detection-run']['wireless-network']))
        time.sleep(0.5)
        for z in content_b['detection-run']['wireless-network']:
            # print(z['BSSID'])
            # print("Check: ", z)
            fltr = list(filter(lambda x:(x["BSSID"]==z['BSSID'])&(x["@last-time"]==z['@last-time']),content_a['detection-run']['wireless-network']))
            # print ("Fnd list: ", len(list(fltr)))
            # print(fltr)
            if (fltr):
                found = fltr[0]
                # print("Found: ", found)
                content_a['detection-run']['wireless-network'].remove(found)



        time.sleep(0.5)
        # print("Delta:", content_a['detection-run']['wireless-network'])
        print("Reduced update size:  ", len(content_a['detection-run']['wireless-network']))
        return content_a
        # print(content_a['detection-run']['wireless-network'][1]['BSSID'])
        # for key, item in content_a['detection-run'].items():
        #     if (key == 'wireless-network'):
        #         for wn in item:
        #             print(wn[0]['BSSID'], ':',  wn[0]['@last-time'])
        #         print(item)
                # print(item[item.index('BSSID')])
                # print(item.get('BSSID', None), ':', item.get('@last-time', None))
                # print(item['BSSID'], ':', item['@last-time'])


# Launch application with supplied arguments
if __name__ == '__main__':
    debug_level = logging.INFO if not env.bool("DEBUG") else logging.DEBUG
    logfilename = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'aird.log')
    logging.basicConfig(filename=logfilename, format='%(asctime)s %(message)s', level=debug_level)

    w = Watcher()
    w.run()
