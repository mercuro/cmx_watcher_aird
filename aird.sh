#!/bin/bash

cd /home/pi/aird
echo "Starting airmon..."
sudo airmon-ng start wlan1
echo "Starting airodump..."
sudo nohup setsid airodump-ng --manufacturer --uptime -w netxml/clients_`date +%Y-%m-%d.%H:%M:%S`.netxml --output-format netxml --write-interval 30 wlan1mon >/dev/null 2>&1 &
echo "Starting aird..."
sudo nohup python3 aird.py >/dev/null 2>&1 &
